//Modules and Parameterized Routes

/*Separation Concerns
	- Routes 
	- Controllers 
	- Schemas 
	- Models
*/

/*Why to separate
	- better code read
	- improved scalability
	- better code maintainability
*/

/* 
Models
	- Contains WHAT objects are needed in our API
		ex: users , courses
	- Object schemas and relationships are defined here

Routes
	- Defines WHEN particular controllers will be used
	- A specific controller action will be called when a specific HTTP method is received on a specific API endpoint.

Controlllers
	- Contains intructions on HOW your API will perform its intended tasks
	- Mongoose model quiries are used here, examples are:
		- model.find()
		- model.findByidAndDelete()
		- model.findByidAndUpdate() and etc...
*/

// We Separate our concerns through the use of JS modules

// CODE ALONG - DISCUSSION

/*
npm init -y
npm install express mongoose
npm install -g nodemon (only install once)
touch index.js .gitignore
in .gitignore.txt >> type [node_modules]

for separation of models, controllers and routes
mkdir controllers models routes


*/

// SETUP the dependencies

const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
// treated as module
const taskRoute = require("./routes/taskRoute");

// Server Setup
const app = express();
const port = 3001;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// DATABASE CONNECTION [START] *****************
mongoose.connect("mongodb+srv://jamepaullincuna:admin123@zuitt-bootcamp.k9jjqkm.mongodb.net/s36?retryWrites=true&w=majority",
	{useNewUrlParser: true,useUnifiedTopology: true}
);
// for handling error 
//mongoose.connection.on("error", console.error.bind(console, "connection error"));
// even if no error handler , .connection will still show error if have
mongoose.connection.once("open", () => console.log("Now connected to the cloud database"));

// DATABASE CONNECTION [ END ] *****************

// Add Task route
// Allows all the task routes create in the "taskRoute.js" file to use "/tasks" route

app.use("/tasks", taskRoute);


app.listen(port, () => console.log(`Now listening to port ${port}.`));

