// Defines WHEN particular controller will be used
// Contain all the endpoints and responses that we can get from controllers

const express = require("express");

// Creates a router instance that functions as middleware and routing system
const router = express.Router();

const taskController = require("../controllers/taskController");

// Route to get all the task
// http://localhost:3001/tasks
router.get("/", (req,res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create task
// http://localhost:3001/tasks
router.post("/", (req,res) => {
	taskController.createTasks(req.body).then(result => res.send(result));
});

// Route to delete task
// "/:id" is a wildcard parameter that set data in req.params.id
router.delete("/:id", (req,res) => {
	taskController.deleteTasks(req.params.id).then(result => res.send(result));
});


// **************************************************************
// ******************** ACTIVITY [START] ************************
// **************************************************************

router.get("/:id", (req,res) => {
	taskController.getSpecificTasks(req.params.id).then(result => res.send(result));
});

router.put("/:id", (req,res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
});

// **************************************************************
// ******************** ACTIVITY [ END ] ************************
// **************************************************************

module.exports = router;