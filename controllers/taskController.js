// Contains instructions on HOW your API will perform its intended tasks
// All the operations it can do will be placed in this file

//model name should be sigular and capitalize the first letter
const Task = require("../models/task");

//----------------------------------

module.exports.getAllTasks = () => {
	return Task.find({}).then(result=>{
		return result;
	})
};

//----------------------------------

// Controller function for create a task

module.exports.createTasks = (requestBody) => {
	
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((nTask,error)=>{
		if (error){
			console.log(error);
			return false;
		} else {
			return nTask;
		}
	})
};

//----------------------------------

// Controller function for deleting a task
// Route to delete task
/*Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTasks = (taskId) => {
	
	return Task.findByIdAndRemove(taskId).then((removedTask,err) =>{
		if (err){
			console.log(err);
			return false;
		} else {
			return "Deleted Task."
		}
	})
};

// **************************************************************
// ******************** ACTIVITY [START] ************************
// **************************************************************


/*module.exports.getSpecificTasks = (taskId) => {
	return Task.findOne({_id:taskId}).then(result=>{
		return result;
	})
};*/

module.exports.getSpecificTasks = (taskId) => {
	return Task.findById(taskId).then( result => result )
};

module.exports.updateTask = (taskId, requestBody) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(err);
			return false;
		}

		result.status = requestBody.status;

		return result.save().then((updatedTask, saveErr) => {
			if (saveErr){
				console.log(saveErr);
				return false;
			} else {
				return "Task updated";
			}
		})
	})
};

// **************************************************************
// ******************** ACTIVITY [ END ] ************************
// **************************************************************
